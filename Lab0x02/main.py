''' @file                   main.py
    @brief                  in that file the main program is executed
    @details			These are the final state machines we used:
				\image html Task_Encoder.jpg "Task Encoder"
				\image html Task_User.jpg "Task User
				These are the Task Diagrams we used:
				\image html Task_Diagram.jpg "Task Diagram

    @author                 Sebastian Bößl, Johannes Frisch
    @date                   October 7, 2021

'''

import task_encoder
import task_user
import shares

if __name__ == '__main__':
    
    #shared variables
    ## @brief sends instruction task from task_user to task_encoder to set the encoder to zero
    #
    get_zero_pos = shares.Queue()
    ## @brief sends instruction from task_user to task_encoder to get the current position
    #
    get_pos = shares.Queue()
    ## @brief sends instruction from task_user to task_encoder to get the delta position from the encoder
    #
    get_del = shares.Queue()
    ## @brief sends instruction from task_user to task_encoder to get the current position and time
    #
    collect_data = shares.Queue()
    ## @brief sends current encoder position and time from task_encoder to task_user
    #
    data_list = shares.Queue()
    ## @brief holds the time how long the data collection has been running
    #
    collect_time = shares.Share()
    ## @brief holds the current encoder position
    #
    pos = shares.Share()
    ## @brief holds the current time
    #
    delta = shares.Share()
    
    #initiating tasks
    encoder = task_encoder.Task_Encoder(1000, get_zero_pos, get_pos, get_del, collect_data, data_list, collect_time, pos, delta)
    user = task_user.Task_User(10000, get_zero_pos, get_pos, get_del, collect_data, data_list, collect_time, pos, delta)
    
    while(True):
        
        try:
            encoder.run()
            user.run()
            
        except KeyboardInterrupt:
            break
        
    print('Program terminating')
