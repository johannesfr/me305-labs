# -*- coding: utf-8 -*-
"""
Created on Thu Sep 23 12:50:32 2021

@author: melab15
"""

#Bottom Up approach inspired from 'codescope.com/course/algorithms-dynamic-programming/'
#Definition of an array to store the alreadz calculated fibonacci numbers
F = [0]*10000

#Definition Fibonacci calculater
def fib(idx):
    F[idx] = 0  
    F[1] = 1
    
    #Filling up the array starting at the bottom till we got to the entered indexed
    for i in range(2, idx+1):
        F[i] = F[i-1] + F[i-2] 
        
    #return fibonacci number
    return F[idx]
    
    

    
#console interface loops   
if __name__ == '__main__':
    
    #main loop to keep the programm running, until qutivar != c
    quitvar = 'c'  
    while quitvar == 'c':
        
        #loop that requests an input and checks if its a valid index
        invalidinput = 1
        while invalidinput:
            
            #index input
            userinput = input('Please enter an index for the desired Fibonacci number: ')
            
            #checking if its valid
            if userinput.isnumeric():
                invalidinput = 0
            else:
                print('Please enter a valid index (Only positive integers allowed)')
        
        #convert input to integer
        idx = int(userinput)
        
        #call fibonacci calcutlator function and printing the result        
        print ('Fibonacci number at ' 'index {:} is {:}.'.format(idx, fib(idx)))
        
        #input to continue/close the program
        quitvar = input('Press \'c\' to continue entering another index or any other key to exit the program ')
    